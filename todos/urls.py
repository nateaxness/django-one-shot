from django.urls import path

from todos.views import (
    show_todos,
    todos_list,
)

urlpatterns = [
    path("", todos_list, name="todo_list_list"),
    path("<int:id>/", show_todos, name="todo_list_detail")
]
