from django.shortcuts import get_object_or_404, redirect, render
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.


def todos_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todolists": todolists
    }
    return render(request, "todos/list.html", context)

# def show_todos(request):
#     todolists = TodoList.objects.all()
#     context = {
#         "todolists": todolists
#     }
#     return render(request, "todos/list.html", context)


def show_todos(request, id):
    todolists = get_object_or_404(TodoList, id=id)
    context = {
        "todolists": todolists
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            todolist = form.save(False)
            todolist.user = request.user
            todolist.save()
            return redirect("todo_list_detail", id=todolist.id)

        # To add something to the model, like setting a user,
        # use something like this:
        #
        # model_instance = form.save(commit=False)
        # model_instance.user = request.user
        # model_instance.save()
        # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm()
        return render(request, "todo_lists/detail.html")
